﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Timers;


namespace ProcessMonitoring
{
    class Program
    {

        public static StreamWriter outputFile = null;
        public static Process[] MPO = null;
        static void Main(string[] args)
        {
            string outputF = GetOutputFilePath();
            outputFile = File.CreateText(outputF);

            MPO = Process.GetProcesses(); // Master Process Object initialized

            Console.WriteLine("Process monitoring started");

            Console.WriteLine("Writing to file: " + outputF);
       
            Timer timer001 = new System.Timers.Timer(500);
            timer001.Elapsed += Timer001_Elapsed;
            timer001.Start();

            Console.ReadKey();

            timer001.Stop();
            outputFile.Flush();
            outputFile.Close();

        }

        private static void Timer001_Elapsed(object sender, ElapsedEventArgs e)
        {
            XProcessCheck();
        }

        public static string AppDir
        {
            get
            {
                return AppDomain.CurrentDomain.BaseDirectory.ToString();
            }
        }

        public static void XProcessCheck()
        {
            List<Process> previous = MPO.ToList();
            MPO = Process.GetProcesses();
            List<Process> current = MPO.ToList();

            var currentIDs = (from x in current
                              select x.Id).ToList() ;

            var previousIDs = (from x in previous
                              select x.Id).ToList();

            current.RemoveAll(currentItem => previousIDs.Contains(currentItem.Id));
            previous.RemoveAll(previousItem => currentIDs.Contains(previousItem.Id));


            foreach(var p in previous)
            {
                WriteProgress(String.Format("{0}:{1}", p.ProcessName, p.Id), true);
            }

            foreach(var c in current)
            {
                WriteProgress(String.Format("{0}:{1}", c.ProcessName, c.Id), false);
            }
            //left on previous --> means that process was killed
            //left on current --> means process was started
            
        }

        public static void WriteProgress(string progressStatus, bool removed)
        {
            string timeStr = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            string outStr = String.Format("[{0}] {1}",timeStr, progressStatus);
            outStr = (removed ? "[-]" : "[+]") + outStr;
            Console.WriteLine(outStr);
            outputFile.WriteLine(outStr);
            outputFile.Flush();
        }

        public static string GetOutputFilePath()
        {
            var timestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            return Path.Combine(Program.AppDir, "ProcessMonitor-" + timestamp + ".txt");
        }
    }
}
